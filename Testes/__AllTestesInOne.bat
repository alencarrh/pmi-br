@echo off
echo ********************************************************************************
echo Se somente mostrar as mensagens com inicio 'SLF4J' nao houve problemas.
echo Caso contrario resultara em um erro.
echo ********************************************************************************

echo ********
echo DefineAllTo1
echo ********

java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 5 true true false false true > resultados\DefineAllTo1\DefineAllTo1_1.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 5 true true false false true > resultados\DefineAllTo1\DefineAllTo1_2.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 5 true true false false true > resultados\DefineAllTo1\DefineAllTo1_3.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 5 true true false false true > resultados\DefineAllTo1\DefineAllTo1_4.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 5 true true false false true > resultados\DefineAllTo1\DefineAllTo1_5.txt

echo ********
echo KERNEL1
echo ********

java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 1 true true false false true > resultados\KERNEL1\KERNEL1_1.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 1 true true false false true > resultados\KERNEL1\KERNEL1_2.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 1 true true false false true > resultados\KERNEL1\KERNEL1_3.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 1 true true false false true > resultados\KERNEL1\KERNEL1_4.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 1 true true false false true > resultados\KERNEL1\KERNEL1_5.txt

echo ********
echo KERNEL2
echo ********

java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 2 true true false false true > resultados\KERNEL2\KERNEL2_1.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 2 true true false false true > resultados\KERNEL2\KERNEL2_2.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 2 true true false false true > resultados\KERNEL2\KERNEL2_3.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 2 true true false false true > resultados\KERNEL2\KERNEL2_4.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 2 true true false false true > resultados\KERNEL2\KERNEL2_5.txt

echo ********
echo KERNEL3
echo ********

java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 3 true true false false true > resultados\KERNEL3\KERNEL3_1.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 3 true true false false true > resultados\KERNEL3\KERNEL3_2.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 3 true true false false true > resultados\KERNEL3\KERNEL3_3.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 3 true true false false true > resultados\KERNEL3\KERNEL3_4.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 3 true true false false true > resultados\KERNEL3\KERNEL3_5.txt

echo ********
echo KERNEL4
echo ********

java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 4 true true false false true > resultados\KERNEL4\KERNEL4_1.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 4 true true false false true > resultados\KERNEL4\KERNEL4_2.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 4 true true false false true > resultados\KERNEL4\KERNEL4_3.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 4 true true false false true > resultados\KERNEL4\KERNEL4_4.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 4 true true false false true > resultados\KERNEL4\KERNEL4_5.txt

echo ********
echo Testes finalizados.....
echo ********

pause

