@echo off
echo ********************************************************************************
echo Se somente mostrar as mensagens com inicio 'SLF4J' nao houve problemas.
echo Caso contrario resultara em um erro.
echo ********************************************************************************


echo ********
echo KERNEL3
echo ********

java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 3 true true false false true > resultados\KERNEL3\KERNEL3_2_1.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 3 true true false false true > resultados\KERNEL3\KERNEL3_2_2.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 3 true true false false true > resultados\KERNEL3\KERNEL3_2_3.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 3 true true false false true > resultados\KERNEL3\KERNEL3_2_4.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 3 true true false false true > resultados\KERNEL3\KERNEL3_2_5.txt

echo ********
echo KERNEL4
echo ********

java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 4 true true false false true 2 > resultados\KERNEL4\KERNEL4_1.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 4 true true false false true 2 > resultados\KERNEL4\KERNEL4_2.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 4 true true false false true 2 > resultados\KERNEL4\KERNEL4_3.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 4 true true false false true 2 > resultados\KERNEL4\KERNEL4_4.txt
java -jar "../target/JenaFinder1.1-1.1-JenaFinder.jar" 1 "../fr.rdf" "../owl-fb.rules" 30 4 true true false false true 2 > resultados\KERNEL4\KERNEL4_5.txt

echo ********
echo Testes finalizados.....
echo ********

pause

