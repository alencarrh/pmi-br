package Finder;

import Classes.TriplaInteger;
import Rules.Rules;
import Triples.Triples;
import com.alencar.JenaTests.TutorialKernels;
import com.nativelibs4java.opencl.*;
import com.nativelibs4java.opencl.CLMem.Usage;
import org.bridj.Pointer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import static org.bridj.Pointer.allocateInts;

public class Finder {

    private Boolean auditarResultados;
    private Boolean mostrarLogs;
    private static Long tempoInicio;
    private static Long tempoFim;
    private static final int DIV_TIME = 1000000;

    private final Triples triples;
    private final Rules rules;

    private final Pointer<Integer> subPtr, predPtr, objPtr, findPtr, maskPtr;
    private Pointer<Integer> outPtr;

    private CLBuffer<Integer> sub, pred, obj, mask, find;
    private CLBuffer<Integer> out;

    public Finder(Triples triples, Rules rules) {
        this.triples = triples;
        this.rules = rules;
        this.subPtr = triples.getPointerTriplaSubject();
        this.predPtr = triples.getPointerTriplaPredicate();
        this.objPtr = triples.getPointerTriplaSubject();
        findPtr = allocateInts(3);
        maskPtr = allocateInts(3);
    }

    public void setMostrarLogs(Boolean mostrarLogs) {
        this.mostrarLogs = mostrarLogs;
    }

    public void setAuditarResultados(Boolean auditarResultados) {
        this.auditarResultados = auditarResultados;
    }

    public void buscarPadroes(Boolean escolherDispositivo) throws IOException {
        CLContext context;
        CLQueue queue;
        if (escolherDispositivo) {
            CLDevice deviceSelecionado = escolherDispositivo();
            context = JavaCL.createContext(null, deviceSelecionado);
        } else {
            context = JavaCL.createBestContext();
        }
        queue = context.createDefaultQueue();
        if (mostrarLogs) {
            buscaComLog(context, queue);
        } else {
            buscaSemLog(context, queue);
        }
    }

    private void buscaComLog(CLContext context, CLQueue queue) throws IOException {
        Map<Integer, ArrayList<TriplaInteger>> mapaPadroes = rules.getTriplasInteger();
        for (Map.Entry<Integer, ArrayList<TriplaInteger>> triplas : mapaPadroes.entrySet()) {
            System.out.println("===> Regra número: " + triplas.getKey() + " <=================================");
            for (TriplaInteger tripla : triplas.getValue()) {
                setarPadraoEMascaraComLog(context, tripla);
                System.out.println("Padrões da regra: " + tripla);
                long umaBusca = buscarPadrao(context, queue);
                System.out.print("[" + umaBusca + " ns]~[" + (umaBusca / DIV_TIME) + " ms] | ");
            }
            if (auditarResultados) {
                System.out.print("\nAuditando resultados...  ");
                long[] resultados = auditarResultados();
                System.out.print("[" + resultados[0] + " ns]~[" + (resultados[0] / DIV_TIME) + "ms]");
                System.out.println("\nMatches: " + resultados[1] + "");
            }
            System.out.println("-----------------------------------------------------\n");
        }
    }

    private void buscaSemLog(CLContext context, CLQueue queue) throws IOException {
        Map<Integer, ArrayList<TriplaInteger>> mapaPadroes = rules.getTriplasInteger();
        for (Map.Entry<Integer, ArrayList<TriplaInteger>> triplas : mapaPadroes.entrySet()) {
            for (TriplaInteger tripla : triplas.getValue()) {
                setarPadraoEMascaraComLog(context, tripla);
                buscarPadrao(context, queue);
            }
        }
    }

    /**
     * Faz a chamada do código do OpenCL.
     *
     * @return tempo de execução em nanosegundos
     * @throws IOException
     */
    private long buscarPadrao(CLContext context, CLQueue queue) throws IOException {
        TutorialKernels kernels = new TutorialKernels(context);
        int[] globalSizes = new int[]{triples.getNumeroDeTriplas()};
        CLEvent addEvt;
        tempoInicio = System.nanoTime();
//        addEvt = kernels.matchPattern(queue, sub, pred, obj, mask, find, out, triples.getNumeroDeTriplas(), globalSizes, null);
//        outPtr = out.read(queue, addEvt); // blocks until matchPattern finished
        return (System.nanoTime() - tempoInicio);
    }

    /**
     * Define o padrão e a mascara de busca
     *
     * @param tripla - TriplaInteger com o padrão de busca.
     */
    private void setarPadraoEMascaraComLog(CLContext context, TriplaInteger tripla) {
        setarPadraoBuscaComLog(tripla);
        setarMascaraComLog(tripla);
        prepararOpenCLComLog(context);
    }

    /**
     * Define o padrão de busca.
     *
     * @param tripla
     */
    private void setarPadraoBuscaComLog(TriplaInteger tripla) {
        findPtr.set(0, tripla.getSujeito());
        findPtr.set(1, tripla.getPredicado());
        findPtr.set(2, tripla.getObjeto());
    }

    /**
     *
     * @param tripla
     */
    private void setarMascaraComLog(TriplaInteger tripla) {
        maskPtr.set(0, tripla.getSujeito() == 0 ? 0 : 1);
        maskPtr.set(1, tripla.getPredicado() == 0 ? 0 : 1);
        maskPtr.set(2, tripla.getObjeto() == 0 ? 0 : 1);
    }

    private void prepararOpenCLComLog(CLContext context) {
        criarBuffersDeEntradaComLog(context);
        criarBuffersDeRetornoComLog(context);
    }

    private void criarBuffersDeEntradaComLog(CLContext context) {
        //criando buffers de entrada
        sub = context.createIntBuffer(Usage.Input, subPtr);
        pred = context.createIntBuffer(Usage.Input, predPtr);
        obj = context.createIntBuffer(Usage.Input, objPtr);
        mask = context.createIntBuffer(Usage.Input, maskPtr);
        find = context.createIntBuffer(Usage.Input, findPtr);
    }

    private void criarBuffersDeRetornoComLog(CLContext context) {
        //criando buffer de retorno
        out = context.createIntBuffer(Usage.Output, triples.getNumeroDeTriplas());
    }

    private CLDevice escolherDispositivo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private long[] auditarResultados() {
        long ind, countMatch = 0;
        Boolean isEqual[] = new Boolean[3];
        String comp[] = new String[3];
        tempoInicio = System.nanoTime();
        for (ind = 0; ind < triples.getNumeroDeTriplas(); ind++) {
            isEqual[0] = subPtr.getIntAtIndex(ind) == findPtr.getIntAtIndex(0);
            isEqual[1] = predPtr.getIntAtIndex(ind) == findPtr.getIntAtIndex(1);
            isEqual[2] = objPtr.getIntAtIndex(ind) == findPtr.getIntAtIndex(2);
            if (outPtr.getIntAtIndex(ind) == 0) {
                countMatch++;
                boolean ok = true;
                if (maskPtr.getIntAtIndex(0) == 1) {
                    ok = isEqual[0];
                }
                if (ok && maskPtr.getIntAtIndex(1) == 1) {
                    ok = isEqual[1];
                }
                if (ok && maskPtr.getIntAtIndex(2) == 1) {
                    ok = isEqual[2];
                }
                if (!ok) {
                    comp[0] = maskPtr.get(0) == 1 ? isEqual[0].toString() : "---";
                    comp[1] = maskPtr.get(1) == 1 ? isEqual[1].toString() : "---";
                    comp[2] = maskPtr.get(2) == 1 ? isEqual[2].toString() : "---";
                    System.out.println("Problemas:");
                    System.out.println("Tripla\t" + subPtr.get(ind) + "\t" + predPtr.get(ind) + "\t" + objPtr.get(ind));
                    System.out.println("Compar\t" + comp[0] + "\t" + comp[1] + "\t" + comp[2]);
                    System.out.println("Output  " + outPtr.get(ind) + "\n");
                }
            } else {
                boolean ok = false;
                if (maskPtr.getIntAtIndex(0) == 1) {
                    ok = !isEqual[0];
                }
                if (!ok && maskPtr.getIntAtIndex(1) == 1) {
                    ok = !isEqual[1];
                }
                if (!ok && maskPtr.getIntAtIndex(2) == 1) {
                    ok = !isEqual[2];
                }
                if (!ok) {
                    comp[0] = maskPtr.get(0) == 1 ? isEqual[0].toString() : "---";
                    comp[1] = maskPtr.get(1) == 1 ? isEqual[1].toString() : "---";
                    comp[2] = maskPtr.get(2) == 1 ? isEqual[2].toString() : "---";
                    System.out.println("Problemas:");
                    System.out.println("Tripla\t" + subPtr.get(ind) + "\t" + predPtr.get(ind) + "\t" + objPtr.get(ind));
                    System.out.println("Compar\t" + comp[0] + "\t" + comp[1] + "\t" + comp[2]);
                    System.out.println("Output  " + outPtr.get(ind) + "\n");
                }
            }
        }
        long tempo = (System.nanoTime() - tempoInicio);
        long retorno[] = new long[2];
        retorno[0] = tempo;
        retorno[1] = countMatch;
        return retorno;
    }
}
