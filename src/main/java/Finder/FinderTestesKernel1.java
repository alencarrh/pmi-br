package Finder;

import Classes.Node;
import Classes.TriplaInteger;
import Rules.Rules;
import Triples.Triples;
import com.alencar.JenaTests.TutorialKernels;
import com.nativelibs4java.opencl.CLBuffer;
import com.nativelibs4java.opencl.CLContext;
import com.nativelibs4java.opencl.CLDevice;
import com.nativelibs4java.opencl.CLEvent;
import com.nativelibs4java.opencl.CLMem.Usage;
import com.nativelibs4java.opencl.CLPlatform;
import com.nativelibs4java.opencl.CLQueue;
import org.bridj.Pointer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.bridj.Pointer.allocateInts;

public class FinderTestesKernel1 implements FinderPai {

    private Boolean auditarResultados;
    private static Long tempoInicio;
    private static final int DIV_TIME = 1000000;

    private final Triples triples;
    private final Rules rules;

    private final Pointer<Integer> subPtr, predPtr, objPtr, findPtr;
    private Pointer<Integer> outPtr;

    private CLBuffer<Integer> sub, pred, obj, find;
    private CLBuffer<Integer> out;

    private CLQueue queue;
    private CLContext context;

    public FinderTestesKernel1(Triples triples, Rules rules) {
        super();
        this.triples = triples;
        this.rules = rules;
        this.subPtr = triples.getPointerTriplaSubject();
        this.predPtr = triples.getPointerTriplaPredicate();
        this.objPtr = triples.getPointerTriplaObject();
        findPtr = allocateInts(3);
//        maskPtr = allocateInts(3);
        auditarResultados = true;
    }

    @Override
    public void setAuditarResultados(Boolean auditarResultados) {
        this.auditarResultados = auditarResultados;
    }

    @Override
    public void executarTestes(Integer nExperimentos, Integer idDrive) throws IOException {
        context = escolherDevice(idDrive);
        queue = context.createDefaultQueue();
        ArrayList<TriplaInteger> test_Triplas = new ArrayList<>();

        int numeroDeTriplas = triples.getNumeroDeTriplas();
        int resource = triples.getArrayDeNos().get(triples.getArrayDeNos().indexOf(new Node(0, "http://schema.org/salaryCurrency"))).id;
        int property = triples.getArrayDeNos().get(triples.getArrayDeNos().indexOf(new Node(0, "http://www.w3.org/2000/01/rdf-schema#range"))).id;
        int object = triples.getArrayDeNos().get(triples.getArrayDeNos().indexOf(new Node(0, "http://www.w3.org/2001/XMLSchema#string"))).id;

        System.out.println("padrão completo: (" + resource + " " + property + " " + object + ")");
        System.out.println("resource(" + (resource) + "): " + triples.getArrayDeNos().get(resource - 1));
        System.out.println("property(" + (property) + "): " + triples.getArrayDeNos().get(property - 1));
        System.out.println("  object(" + (object) + "): " + triples.getArrayDeNos().get(object - 1));
        System.out.println("");
//        System.exit(0);
        //tipo 1 ----------------------------------------------------------
        //
        //tripla (any, any, any)
        TriplaInteger test_temp = new TriplaInteger();
        test_temp.setSujeito(0);
        test_temp.setPredicado(0);
        test_temp.setObjeto(0);
        //adiciona tripla 1
        test_Triplas.add(test_temp);
        //
        //tipo 1 ----------------------------------------------------------
        //
        //tipo 2 ----------------------------------------------------------
        //
        //tripla (ALEATÓRIO, any, any)
        test_temp = new TriplaInteger();
        test_temp.setSujeito(resource);
        test_temp.setPredicado(0);
        test_temp.setObjeto(0);
        //adiciona tripla 2.1
        test_Triplas.add(test_temp);
        //
        //tripla (any, ALEATÓRIO, any)
        test_temp = new TriplaInteger();
        test_temp.setSujeito(0);
        test_temp.setPredicado(property);
        test_temp.setObjeto(0);
        //adiciona tripla 2.2
        test_Triplas.add(test_temp);
        //
        //tripla (any, any, ALEATÓRIO)
        test_temp = new TriplaInteger();
        test_temp.setSujeito(0);
        test_temp.setPredicado(0);
        test_temp.setObjeto(object);
        //adiciona tripla 2.3
        test_Triplas.add(test_temp);
        //
        //tipo 2 ----------------------------------------------------------
        //
        //tipo 3 ----------------------------------------------------------
        //
        //tripla (ALEATÓRIO, ALEATÓRIO, any)
        test_temp = new TriplaInteger();
        test_temp.setSujeito(resource);
        test_temp.setPredicado(property);
        test_temp.setObjeto(0);
        //adiciona tripla 3.1
        test_Triplas.add(test_temp);
        //
        //tripla (ALEATÓRIO, any, ALEATÓRIO)
        test_temp = new TriplaInteger();
        test_temp.setSujeito(resource);
        test_temp.setPredicado(0);
        test_temp.setObjeto(object);
        //adiciona tripla 3.2
        test_Triplas.add(test_temp);
        //tripla (any, ALEATÓRIO, ALEATÓRIO)
        test_temp = new TriplaInteger();
        test_temp.setSujeito(0);
        test_temp.setPredicado(property);
        test_temp.setObjeto(object);
        //adiciona tripla 3.3
        test_Triplas.add(test_temp);

        //
        //tipo 3 ----------------------------------------------------------
        //
        //tripla (ALEATÓRIO, ALEATÓRIO, ALEATÓRIO)
        test_temp = new TriplaInteger();
        test_temp.setSujeito(resource);
        test_temp.setPredicado(property);
        test_temp.setObjeto(object);

        //adiciona tripla 4
        test_Triplas.add(test_temp);

        System.out.println("Iniciando testes ---------------------------------------------");
        System.out.println("Número de triplas: " + numeroDeTriplas);
        System.out.println("Número de  regras: " + rules.getNumeroDeRegras());
        for (TriplaInteger test_Tripla : test_Triplas) {
            setarPadraoEMascara(test_Tripla);
            System.out.println("");
            System.out.println("==================> Padrão de busca.: " + test_Tripla + "");
//            System.out.println("==================> Máscara de busca: (" + maskPtr.get(0) + " " + maskPtr.get(1) + " " + maskPtr.get(2) + ")");
            long total = 0;
            for (int j = 0; j < nExperimentos; j++) {
                long umaBusca = buscarPadrao();
//                System.out.println(umaBusca + "ns ["+(umaBusca/DIV_TIME)+"ms]");
                System.out.println(umaBusca);
                total += umaBusca;
            }
            if (auditarResultados) {
                System.out.println("");
                System.out.println("");
                System.out.print("Auditando resultados...  ");
                long[] resultados = auditarResultados();
                System.out.print("[" + resultados[0] + " ns]~[" + (resultados[0] / DIV_TIME) + "ms]");
                System.out.println("");
                System.out.println("Matches: " + resultados[1] + "");
            }
            System.out.println("Média dos " + nExperimentos + " experimentos: [" + (total / nExperimentos) + " ns] ~ [" + ((total / nExperimentos) / DIV_TIME) + " ms]");
            System.out.println("---------------------------------------------");

        }
    }

    /**
     * Faz a chamada do código do OpenCL.
     *
     * @return tempo de execução em nanosegundos
     * @throws IOException
     */
    private long buscarPadrao() throws IOException {
        TutorialKernels kernels = new TutorialKernels(context);
        int[] globalSizes = new int[]{triples.getNumeroDeTriplas()};
        CLEvent addEvt;
        tempoInicio = System.nanoTime();
        addEvt = kernels.matchPatternKernel1(queue, sub, pred, obj, find, out, triples.getNumeroDeTriplas(), globalSizes, null);
        outPtr = out.read(queue, addEvt); // blocks until matchPattern finished
        long tempoFim = System.nanoTime();
        long t = tempoFim - tempoInicio;
        return t;
    }

    /**
     * Define o padrão e a mascara de busca
     *
     * @param tripla - TriplaInteger com o padrão de busca.
     */
    private void setarPadraoEMascara(TriplaInteger tripla) {
        setarPadraoBusca(tripla);
        setarMascara(tripla);
        prepararOpenCL();
    }

    /**
     * Define o padrão de busca.
     *
     * @param tripla
     */
    private void setarPadraoBusca(TriplaInteger tripla) {
        findPtr.set(0, tripla.getSujeito());
        findPtr.set(1, tripla.getPredicado());
        findPtr.set(2, tripla.getObjeto());
    }

    /**
     *
     * @param tripla
     */
    private void setarMascara(TriplaInteger tripla) {
    }

    private void prepararOpenCL() {
        criarBuffersDeEntrada();
        criarBuffersDeRetorno();
    }

    private void criarBuffersDeEntrada() {
        //criando buffers de entrada
        sub = context.createIntBuffer(Usage.Input, subPtr);
        pred = context.createIntBuffer(Usage.Input, predPtr);
        obj = context.createIntBuffer(Usage.Input, objPtr);
        find = context.createIntBuffer(Usage.Input, findPtr);
    }

    private void criarBuffersDeRetorno() {
        //criando buffer de retorno
        out = context.createIntBuffer(Usage.Output, triples.getNumeroDeTriplas());
    }

    private CLContext escolherDevice(int op) {
        List<CLDevice> devicesParaEscolher = new ArrayList<>();
        for (CLPlatform platform : com.nativelibs4java.opencl.JavaCL.listPlatforms()) {
            devicesParaEscolher.addAll(Arrays.asList(platform.listAllDevices(false)));
        }

        //seleciona o dispositivo
        System.out.println("Dispositivos:  ");
        for (int i = 0; i < devicesParaEscolher.size(); i++) {
            System.out.println(i + " - " + devicesParaEscolher.get(i).getName());
        }
        System.out.println("");
        System.out.println("");
        //cria o contexto atual com o drive selecionado.
        CLContext tempContext;
        if (op == -1) {
            tempContext = com.nativelibs4java.opencl.JavaCL.createBestContext();
        } else {
            CLDevice deviceSelecionado = devicesParaEscolher.get(op);
            tempContext = com.nativelibs4java.opencl.JavaCL.createContext(null, deviceSelecionado);
        }

        //printa o nome do dispositivo escolhido...
        System.out.println("Dispositivo selecionado: " + tempContext.getDevices()[0].getName());
        System.out.println("");

        return tempContext;
    }

    private long[] auditarResultados() {
        long retorno[] = new long[2];
        int count = 0;
        tempoInicio = System.nanoTime();
        for (int i = 0; i < triples.getNumeroDeTriplas(); i++) {
//            System.out.println("temp: " + outPtr.get(i));
            if (outPtr.get(i) == 0) {
                count++;
            }
        }
        long tempoFim = System.nanoTime();
        retorno[0] = (tempoFim - tempoInicio);
        retorno[1] = count;
        return retorno;
    }
}
