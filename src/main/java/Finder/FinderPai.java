package Finder;

import java.io.IOException;

/**
 *
 * @author Alencar Rodrigo Hentges <alencarhentges@gmail.com>
 * @date 10/08/2016 - 20:28:46
 */
public interface FinderPai {

    public void setAuditarResultados(Boolean auditarResultados);

    public void executarTestes(Integer nExperimentos, Integer idDrive) throws IOException;

}
