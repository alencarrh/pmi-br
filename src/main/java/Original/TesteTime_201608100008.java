package Original;

import java.io.File;
import java.util.List;
import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.reasoner.rulesys.GenericRuleReasoner;
import org.apache.jena.reasoner.rulesys.Rule;

/**
 *
 * @author Alencar Rodrigo Hentges <alencarhentges@gmail.com>
 */
public class TesteTime_201608100008 {

    public static void executaOriginal(String ARQUIVO_TRIPLAS, String ARQUIVO_REGRAS, Integer nExperimentos) throws Exception {
        Model m = ModelFactory.createDefaultModel();
        String input;
        File f;
        long inicio, fim, divTime = 1000000;

        //load model
//        input = "storeFront.owl";
        input = ARQUIVO_TRIPLAS;
        f = new File(input);
        if (f.exists() && f.isFile()) {
            System.out.println("");
            System.out.println("");
            System.out.print("Lendo triplas de '" + input + "'...");
            inicio = System.nanoTime();

            m.read("file:" + input);

            fim = System.nanoTime();
            System.out.print(" [" + (fim - inicio) + "ns] [" + ((fim - inicio) / divTime) + "ms]");
            System.out.println("");
        } else {
            throw new Exception("Bad file location");
        }

        //load rules
        input = ARQUIVO_REGRAS;
//        input = "storefront.rules";
        f = new File(input);
        if (f.exists()) {
            System.out.print("Lendo regras de '" + input + "'...");
            inicio = System.nanoTime();

            List<Rule> rules = Rule.rulesFromURL("file:" + input);

            fim = System.nanoTime();
            System.out.print(" [" + (fim - inicio) + "ns] [" + ((fim - inicio) / divTime) + "ms]");
            System.out.println("");

            System.out.print("Criando Reasoner...");
            inicio = System.nanoTime();

            GenericRuleReasoner r = new GenericRuleReasoner(rules);
            r.setOWLTranslation(true);
            r.setTransitiveClosureCaching(true);

            fim = System.nanoTime();
            System.out.print(" [" + (fim - inicio) + "ns] [" + ((fim - inicio) / divTime) + "ms]");
            System.out.println("");

            System.out.print("Criando InfModel...");
            inicio = System.nanoTime();
            InfModel infmodel = ModelFactory.createInfModel(r, m);

            fim = System.nanoTime();
            System.out.print("[" + (fim - inicio) + "ns] [" + ((fim - inicio) / divTime) + "ms]");
            System.out.println("");

//            m.add(infmodel.getDeductionsModel());
        } else {
            throw new Exception("That rules file does not exist.");
        }
        int i;
//        Resource resource = m.getResource("http://storeFront.com#hasPurchased");
//        Property property = m.getProperty("http://www.w3.org/2002/07/owl#inverseOf");
//        RDFNode object = m.getResource("http://storeFront.com#wasPurchasedBy");
        Resource resource = m.getResource("http://schema.org/salaryCurrency");
        Property property = m.getProperty("http://www.w3.org/2000/01/rdf-schema#range");
        RDFNode object = m.getResource("http://www.w3.org/2001/XMLSchema#string");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        StmtIterator stmt = null;

        long tempoInicio;
        long tempoFim;
        long tempoTotal;
        int DIV_TIME = 1000000;
        //        
        System.out.println("Padrão de busca(");
        System.out.println("| resource: " + resource.toString());
        System.out.println("| property: " + property.toString());
        System.out.println("| object..: " + object.toString());
        System.out.println(")");
        //        
        //BUSCA 1 --------------------------------------------------------
        tempoTotal = 0;
        System.out.println("Padrão 1 ----------------------");
        System.out.println("==================> Máscara de busca: (0 0 0)");
        for (int j = 0; j < nExperimentos; j++) {
            tempoInicio = System.nanoTime();
            stmt = m.listStatements(null, null, (RDFNode) null);
            tempoFim = System.nanoTime();
            long t = (tempoFim - tempoInicio);
            System.out.print("[" + t + "ns] [" + (t / 1000000) + "ms] | ");
            tempoTotal += t;
        }
        for (i = 0; stmt.hasNext(); i++) {
            stmt.nextStatement();
        }
        System.out.println("");
        System.out.println("");        
        System.out.println("Média dos " + nExperimentos + " experimentos: [" + (tempoTotal / nExperimentos) + " ns] ~ [" + ((tempoTotal / nExperimentos) / DIV_TIME) + " ms]");
        System.out.println("Matches: " + i + "");
        System.out.println("");
        //BUSCA 1 --------------------------------------------------------
        //        
        //        
        //BUSCA 2 --------------------------------------------------------
        tempoTotal = 0;
        System.out.println("Padrão 2.1 ----------------------");
        System.out.println("==================> Máscara de busca: (1 0 0)");
        for (int j = 0; j < nExperimentos; j++) {
            tempoInicio = System.nanoTime();
            stmt = m.listStatements(resource, null, (String) null);
            tempoFim = System.nanoTime();
            long t = (tempoFim - tempoInicio);
            System.out.print("[" + t + "ns] [" + (t / 1000000) + "ms] | ");
            tempoTotal += t;
        }
        for (i = 0; stmt.hasNext(); i++) {
            stmt.nextStatement();
        }
        System.out.println("");
        System.out.println("");
        System.out.println("Média dos " + nExperimentos + " experimentos: [" + (tempoTotal / nExperimentos) + " ns] ~ [" + ((tempoTotal / nExperimentos) / DIV_TIME) + " ms]");
        System.out.println("Matches: " + i + "");
        System.out.println("");
        //        
        tempoTotal = 0;
        //        
        System.out.println("Padrão 2.2 ----------------------");
        System.out.println("==================> Máscara de busca: (0 1 0)");
        for (int j = 0; j < nExperimentos; j++) {
            tempoInicio = System.nanoTime();
            stmt = m.listStatements(null, property, (String) null);
            tempoFim = System.nanoTime();
            long t = (tempoFim - tempoInicio);
            System.out.print("[" + t + "ns] [" + (t / 1000000) + "ms] | ");
            tempoTotal += t;
        }
        for (i = 0; stmt.hasNext(); i++) {
            stmt.nextStatement();
        }
        System.out.println("");
        System.out.println("");
        System.out.println("Média dos " + nExperimentos + " experimentos: [" + (tempoTotal / nExperimentos) + " ns] ~ [" + ((tempoTotal / nExperimentos) / DIV_TIME) + " ms]");
        System.out.println("Matches: " + i + "");
        System.out.println("");
        //        
        tempoTotal = 0;
        //
        System.out.println("Padrão 2.3 ----------------------");
        System.out.println("==================> Máscara de busca: (0 0 1)");
        for (int j = 0; j < nExperimentos; j++) {
            tempoInicio = System.nanoTime();
            stmt = m.listStatements(null, null, object);
            tempoFim = System.nanoTime();
            long t = (tempoFim - tempoInicio);
            System.out.print("[" + t + "ns] [" + (t / 1000000) + "ms] | ");
            tempoTotal += t;
        }
        for (i = 0; stmt.hasNext(); i++) {
            stmt.nextStatement();
        }
        System.out.println("");
        System.out.println("");
        System.out.println("Média dos " + nExperimentos + " experimentos: [" + (tempoTotal / nExperimentos) + " ns] ~ [" + ((tempoTotal / nExperimentos) / DIV_TIME) + " ms]");
        System.out.println("Matches: " + i + "");
        System.out.println("");
        //BUSCA 2 --------------------------------------------------------
        //
        //        
        //BUSCA 3 --------------------------------------------------------
        tempoTotal = 0;
        System.out.println("Padrão 3.1 ----------------------");
        System.out.println("==================> Máscara de busca: (1 1 0)");
        for (int j = 0; j < nExperimentos; j++) {
            tempoInicio = System.nanoTime();
            stmt = m.listStatements(resource, property, (RDFNode) null);
            tempoFim = System.nanoTime();
            long t = (tempoFim - tempoInicio);
            System.out.print("[" + t + "ns] [" + (t / 1000000) + "ms] | ");
            tempoTotal += t;
        }
        for (i = 0; stmt.hasNext(); i++) {
            stmt.nextStatement();
        }
        System.out.println("");
        System.out.println("");
        System.out.println("Média dos " + nExperimentos + " experimentos: [" + (tempoTotal / nExperimentos) + " ns] ~ [" + ((tempoTotal / nExperimentos) / DIV_TIME) + " ms]");
        System.out.println("Matches: " + i + "");
        System.out.println("");
        //        
        tempoTotal = 0;
        //
        System.out.println("Padrão 3.2 ----------------------");
        System.out.println("==================> Máscara de busca: (1 0 1)");
        for (int j = 0; j < nExperimentos; j++) {
            tempoInicio = System.nanoTime();
            stmt = m.listStatements(resource, null, object);
            tempoFim = System.nanoTime();
            long t = (tempoFim - tempoInicio);
            System.out.print("[" + t + "ns] [" + (t / 1000000) + "ms] | ");
            tempoTotal += t;
        }
        for (i = 0; stmt.hasNext(); i++) {
            stmt.nextStatement();
        }
        System.out.println("");
        System.out.println("");
        System.out.println("Média dos " + nExperimentos + " experimentos: [" + (tempoTotal / nExperimentos) + " ns] ~ [" + ((tempoTotal / nExperimentos) / DIV_TIME) + " ms]");
        System.out.println("Matches: " + i + "");
        System.out.println("");
        //        
        tempoTotal = 0;
        //
        System.out.println("Padrão 3.3 ----------------------");
        System.out.println("==================> Máscara de busca: (0 1 1)");
        for (int j = 0; j < nExperimentos; j++) {
            tempoInicio = System.nanoTime();
            stmt = m.listStatements(null, property, object);
            tempoFim = System.nanoTime();
            long t = (tempoFim - tempoInicio);
            System.out.print("[" + t + "ns] [" + (t / 1000000) + "ms] | ");
            tempoTotal += t;
        }
        for (i = 0; stmt.hasNext(); i++) {
            stmt.nextStatement();
        }
        System.out.println("");
        System.out.println("");
        System.out.println("Média dos " + nExperimentos + " experimentos: [" + (tempoTotal / nExperimentos) + " ns] ~ [" + ((tempoTotal / nExperimentos) / DIV_TIME) + " ms]");
        System.out.println("Matches: " + i + "");
        System.out.println("");
        //BUSCA 3 --------------------------------------------------------
        //
        //        
        //BUSCA 4 --------------------------------------------------------
        tempoTotal = 0;
        System.out.println("Padrão 4  ----------------------");
        System.out.println("==================> Máscara de busca: (1 1 1)");
        for (int j = 0; j < nExperimentos; j++) {
            tempoInicio = System.nanoTime();
            stmt = m.listStatements(resource, property, object);
            tempoFim = System.nanoTime();
            long t = (tempoFim - tempoInicio);
            System.out.print("[" + t + "ns] [" + (t / 1000000) + "ms] | ");
            tempoTotal += t;
        }
        for (i = 0; stmt.hasNext(); i++) {
            stmt.nextStatement();
        }
        System.out.println("");
        System.out.println("");
        System.out.println("Média dos " + nExperimentos + " experimentos: [" + (tempoTotal / nExperimentos) + " ns] ~ [" + ((tempoTotal / nExperimentos) / DIV_TIME) + " ms]");
        System.out.println("Matches: " + i + "");
        System.out.println("");
    }

}
