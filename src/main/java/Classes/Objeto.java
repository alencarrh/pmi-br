package Classes;

import org.apache.jena.rdf.model.RDFNode;

/**
 *
 * @author Alencar Rodrigo Hentges <alencarhentges@gmail.com>
 */
public class Objeto extends TriplasRDF {

    public RDFNode object;

    public Objeto(int id, RDFNode object) {
        super(id);
        this.object = object;
    }

    @Override
    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        return obj.toString().equals(this.toString());
    }

    @Override
    public String toString() {
        return object.toString();
    }
}
