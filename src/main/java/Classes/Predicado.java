package Classes;

import org.apache.jena.rdf.model.Property;

/**
 *
 * @author Alencar Rodrigo Hentges <alencarhentges@gmail.com>
 */
public class Predicado extends TriplasRDF {

    public Property predicate;

    public Predicado(int id, Property predicate) {
        super(id);
        this.predicate = predicate;
    }

    @Override
    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        
        return obj.toString().equals(this.toString());
    }

    

    @Override
    public String toString() {
        return predicate.toString();
    }
}
