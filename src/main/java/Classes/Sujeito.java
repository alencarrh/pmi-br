
package Classes;

import org.apache.jena.rdf.model.Resource;

/**
 *
 * @author Alencar Rodrigo Hentges <alencarhentges@gmail.com>
 * 
 */
public class Sujeito extends TriplasRDF {

    public Resource subject;

    public Sujeito(int id, Resource subject) {
        super(id);
        this.subject = subject;
    }

    @Override
    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        return obj.toString().equals(this.toString());
    }

    @Override
    public String toString() {
        return subject.toString();
    }
}
