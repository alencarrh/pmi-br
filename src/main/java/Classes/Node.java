package Classes;

/**
 *
 * @author Alencar
 */
public class Node extends TriplasRDF {

    public String node;

    public Node(int id, String node) {
        super(id);
        this.node = node;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        return obj.toString().equals(this.toString());
    }

    @Override
    public String toString() {
        return node;
    }

}
