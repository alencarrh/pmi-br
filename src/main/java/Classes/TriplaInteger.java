package Classes;

/**
 *
 * @author Alencar Rodrigo Hentges <alencarhentges@gmail.com>
 */
public class TriplaInteger {

    private Integer sujeito;
    private Integer predicado;
    private Integer objeto;

    public TriplaInteger(Integer sujeito, Integer predicado, Integer objeto) {
        this.sujeito = sujeito;
        this.predicado = predicado;
        this.objeto = objeto;
    }

    public TriplaInteger() {
    }

    public Integer getSujeito() {
        return sujeito;
    }

    public void setSujeito(Integer sujeito) {
        this.sujeito = sujeito;
    }

    public Integer getPredicado() {
        return predicado;
    }

    public void setPredicado(Integer predicado) {
        this.predicado = predicado;
    }

    public Integer getObjeto() {
        return objeto;
    }

    public void setObjeto(Integer objeto) {
        this.objeto = objeto;
    }

    @Override
    public String toString() {
        return "(" + sujeito + " " + predicado + " " + objeto + ")";
    }

}
