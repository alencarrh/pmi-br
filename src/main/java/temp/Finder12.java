package temp;

import com.alencar.JenaTests.TutorialKernels;
import com.nativelibs4java.opencl.*;
import com.nativelibs4java.opencl.CLMem.Usage;
import org.bridj.Pointer;
import static org.bridj.Pointer.*;
import static java.lang.Math.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Finder12 {

    static Pointer<Integer> subPtr, predPtr, objPtr, findPtr, maskPtr, outPtr;
    static int outVtr[];
    static int nTriples = 15000000;

    public static void main(String[] args) throws IOException {
        List<CLDevice> device1s = new ArrayList<CLDevice>();
        for (CLPlatform platform : com.nativelibs4java.opencl.JavaCL.listPlatforms()) {
            for (CLDevice device : platform.listAllDevices(true)) {
                device1s.add(device);
            }
        }

        CLContext context;
        try {
            CLDevice deviceUtilizado = selectDevice(device1s);
            context = com.nativelibs4java.opencl.JavaCL.createContext(null, deviceUtilizado);
        } catch (Exception e) {
            context = com.nativelibs4java.opencl.JavaCL.createBestContext();
        }
        CLDevice devicesFromBestContext[] = context.getDevices();
        String devicesUtilizados = "";
        for (CLDevice device : devicesFromBestContext) {
            devicesUtilizados += " -> " + device.getName() + "\n";
        }
        System.out.println("Device selecionado: " + devicesUtilizados);

        CLQueue queue = context.createDefaultQueue();

        long startTime, estimatedTime, acumTime, divTime = 1000000;

        int nNodes = 4;
        int i;
        int nExperimentos = 25;

        outVtr = new int[nTriples];

        System.out.print("Alocando " + nTriples + " de triplas ...");
        startTime = System.nanoTime();
        subPtr = allocateInts(nTriples);
        predPtr = allocateInts(nTriples);
        objPtr = allocateInts(nTriples);
        estimatedTime = System.nanoTime() - startTime;
        System.out.println(estimatedTime / divTime + " ms");

        System.out.print("Preenchendo triplas com valores aleatorios...");
        startTime = System.nanoTime();
        for (i = 0; i < nTriples; i++) {
            subPtr.set(i, (int) (Math.random() * nNodes) + 1);
            predPtr.set(i, (int) (Math.random() * nNodes) + 1);
            objPtr.set(i, (int) (Math.random() * nNodes) + 1);
        }
        estimatedTime = System.nanoTime() - startTime;
        System.out.println(estimatedTime / divTime + " ms");

        System.out.println("Preenchendo dados de busca com valores aleatorios...");
        findPtr = allocateInts(3);
        for (i = 0; i < 3; i++) {
            findPtr.set(i, (int) (Math.random() * nNodes) + 1);
        }

        System.out.println("Preenchendo mascara de busca com valores aleatorios...");
        maskPtr = allocateInts(3);
        // 0 => irrelevante
        // 1 => relevante
        for (i = 0; i < 3; i++) {
            //maskPtr.set(i, (int) (Math.random() * 2));
            maskPtr.set(i, 1);
        }

        System.out.println("Dados para busca gerados:");
        System.out.println("Dados a buscar..:\t" + findPtr.get(0) + "\t" + findPtr.get(1) + "\t" + findPtr.get(2));
        System.out.println("Máscara de busca:\t" + maskPtr.get(0) + "\t" + maskPtr.get(1) + "\t" + maskPtr.get(2));

        acumTime = 0;
        System.out.print("\n\n=========> Busca sequencial\nTempos: ");
        for (i = 0; i < nExperimentos; i++) {
            startTime = System.nanoTime();
            for (int j = 0; j < nTriples; j++) {
                outVtr[j] = 0;
                if (maskPtr.get(0) != 0) {
                    outVtr[j] += abs(subPtr.get(j) - findPtr.get(0));
                }
                if (maskPtr.get(1) != 0) {
                    outVtr[j] += abs(predPtr.get(j) - findPtr.get(1));
                }
                if (maskPtr.get(2) != 0) {
                    outVtr[j] += abs(objPtr.get(j) - findPtr.get(2));
                }

            }
            estimatedTime = System.nanoTime() - startTime;
            acumTime += estimatedTime;
            System.out.print(estimatedTime / divTime + "ms ");
        }
        System.out.println("\n=========> Média dos " + nExperimentos + " experimentos: "
                + acumTime / i / divTime + " ms\n\n");

        Runnable compileRunnable;
        int nThreads;
        nThreads = Runtime.getRuntime().availableProcessors() * 10;
        int incChunk = 3500;
        System.out.println("=========> Threads - tam. pool ativo: "
                + nThreads /*+ " - Bloco: " + incChunk*/);
        System.out.print("Tempos: ");
        acumTime = 0;
        for (i = 0; i < nExperimentos; i++) {
            ExecutorService es = Executors.newFixedThreadPool(nThreads);
            //System.out.print("=========> Busca com Threads #" + i + " iniciada...");
            startTime = System.nanoTime();
            for (int nChunk = 0; nChunk < nTriples; nChunk += incChunk) {
                final int iniBloco = nChunk;
                final int incBloco = incChunk;
                compileRunnable = new Runnable() {
                    @Override
                    public void run() {
                        int fimInc = iniBloco + incBloco;
                        if (nTriples < fimInc) {
                            fimInc = nTriples;
                        }
                        for (int j = iniBloco; j < fimInc; j++) {
                            outVtr[j] = 0;
                            if (maskPtr.get(0) != 0) {
                                outVtr[j] += abs(subPtr.get(j) - findPtr.get(0));
                            }
                            if (maskPtr.get(1) != 0) {
                                outVtr[j] += abs(predPtr.get(j) - findPtr.get(1));
                            }
                            if (maskPtr.get(2) != 0) {
                                outVtr[j] += abs(objPtr.get(j) - findPtr.get(2));
                            }
                        }
                    }
                };
                es.execute(compileRunnable);
            }
            es.shutdown();
            try {
                es.awaitTermination(2, TimeUnit.MINUTES);
            } catch (InterruptedException ex) {
                System.out.println("timed out!");
            }
            estimatedTime = System.nanoTime() - startTime;
            acumTime += estimatedTime;
            System.out.print(estimatedTime / divTime + "ms ");
        }
        System.out.println("\n=========> Média dos " + nExperimentos + " experimentos: "
                + acumTime / i / divTime + " ms\n\n");

        System.out.println("=========> Iniciando procedimentos para uso do OpenCL...");
        System.out.print("Criando buffers de entrada...");
        CLBuffer<Integer> sub, pred, obj, mask, find;
        startTime = System.nanoTime();
        sub = context.createIntBuffer(Usage.Input, subPtr);
        pred = context.createIntBuffer(Usage.Input, predPtr);
        obj = context.createIntBuffer(Usage.Input, objPtr);
        mask = context.createIntBuffer(Usage.Input, maskPtr);
        find = context.createIntBuffer(Usage.Input, findPtr);
        estimatedTime = System.nanoTime() - startTime;
        System.out.println(estimatedTime / divTime + " ms");

        CLBuffer<Integer> out;
        System.out.print("Criando buffer de retorno...");
        startTime = System.nanoTime();
        out = context.createIntBuffer(Usage.Output, nTriples);
        estimatedTime = System.nanoTime() - startTime;
        System.out.println(estimatedTime + " ns");

        TutorialKernels kernels = new TutorialKernels(context);
        int[] globalSizes = new int[]{nTriples};
        CLEvent addEvt;
        acumTime = 0;
        System.out.print("=========> Busca OpenCL\nTempos: ");
        for (i = 0; i < nExperimentos; i++) {
            startTime = System.nanoTime();
//            addEvt = kernels.matchPattern(queue, sub, pred, obj, mask, find, out, nTriples, globalSizes, null);
//            outPtr = out.read(queue, addEvt); // blocks until add_floats finished
            estimatedTime = System.nanoTime() - startTime;
            acumTime += estimatedTime;
            System.out.print(estimatedTime / divTime + "ms ");
        }
        System.out.println("\n=========> Média dos " + nExperimentos + " experimentos: " + acumTime / i / divTime + " ms\n\n");

        System.out.print("Auditando resultados...");
        long ind, countMatch = 0;
        Boolean isEqual[] = new Boolean[3];
        String comp[] = new String[3];
        startTime = System.nanoTime();
        for (ind = 0; ind < nTriples; ind++) {
            isEqual[0] = subPtr.getIntAtIndex(ind) == findPtr.getIntAtIndex(0);
            isEqual[1] = predPtr.getIntAtIndex(ind) == findPtr.getIntAtIndex(1);
            isEqual[2] = objPtr.getIntAtIndex(ind) == findPtr.getIntAtIndex(2);
            if (outPtr.getIntAtIndex(ind) == 0) {
                countMatch++;
                boolean ok = true;
                if (maskPtr.getIntAtIndex(0) == 1) {
                    ok = isEqual[0];
                }
                if (ok && maskPtr.getIntAtIndex(1) == 1) {
                    ok = isEqual[1];
                }
                if (ok && maskPtr.getIntAtIndex(2) == 1) {
                    ok = isEqual[2];
                }
                if (!ok) {
                    comp[0] = maskPtr.get(0) == 1 ? isEqual[0].toString() : "---";
                    comp[1] = maskPtr.get(1) == 1 ? isEqual[1].toString() : "---";
                    comp[2] = maskPtr.get(2) == 1 ? isEqual[2].toString() : "---";
                    System.out.println("Problemas:");
                    System.out.println("Tripla\t" + subPtr.get(ind) + "\t" + predPtr.get(ind) + "\t" + objPtr.get(ind));
                    System.out.println("Compar\t" + comp[0] + "\t" + comp[1] + "\t" + comp[2]);
                    System.out.println("Output  " + outPtr.get(ind) + "\n");
                }
            } else {
                boolean ok = false;
                if (maskPtr.getIntAtIndex(0) == 1) {
                    ok = !isEqual[0];
                }
                if (!ok && maskPtr.getIntAtIndex(1) == 1) {
                    ok = !isEqual[1];
                }
                if (!ok && maskPtr.getIntAtIndex(2) == 1) {
                    ok = !isEqual[2];
                }
                if (!ok) {
                    comp[0] = maskPtr.get(0) == 1 ? isEqual[0].toString() : "---";
                    comp[1] = maskPtr.get(1) == 1 ? isEqual[1].toString() : "---";
                    comp[2] = maskPtr.get(2) == 1 ? isEqual[2].toString() : "---";
                    System.out.println("Problemas:");
                    System.out.println("Tripla\t" + subPtr.get(ind) + "\t" + predPtr.get(ind) + "\t" + objPtr.get(ind));
                    System.out.println("Compar\t" + comp[0] + "\t" + comp[1] + "\t" + comp[2]);
                    System.out.println("Output  " + outPtr.get(ind) + "\n");
                }
            }
        }
        estimatedTime = System.nanoTime() - startTime;
        System.out.println(estimatedTime / divTime + " ms");
        System.out.println("Verificadas " + ind + " triplas (" + countMatch + " matches).");

    }

    private static CLDevice selectDevice(List<CLDevice> devices) {
        Scanner ler = new Scanner(System.in);
        System.out.println("Devices found: ");
        int i = 0;
        for (CLDevice device : devices) {
            System.out.println((i++) + " -> " + device.getName());
        }
        System.out.print("\nChoose one: ");
        String index = ler.next();
        return devices.get(Integer.valueOf(index));
    }
}
