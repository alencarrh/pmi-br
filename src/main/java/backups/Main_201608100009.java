package backups;

import Finder.FinderTestesKernel1;
import Rules.Rules;
import Triples.Triples;

import java.io.IOException;

/**
 *
 * @author Alencar Rodrigo Hentges <alencarhentges@gmail.com>
 * @date 26/06/2016 - 16:14:21
 */
public class Main_201608100009 {

    //campos obrigatórios nos dois casos
    private static String ARQUIVO_TRIPLAS;
    private static String ARQUIVO_REGRAS;
    private static Integer EXECUCOES;
    //---------------------------------------

    //campos para algoritmo vetorizado
    private static Boolean TRIPLAS_LOG;
    private static Boolean REGRAS_LOG;
    private static Boolean REGRAS_PRINTAR;
    private static Boolean PRINTAR_TRIPLAS_AND_REGRAS;
    private static Boolean FINDER_AUDITAR;
    private static Integer DISPOSITIVO_CODIGO;

    public static void main(String[] args) throws IOException, Exception {
        if (args.length == 0) {
            throw new Exception("É necessário passar argumentos");
        }
        obterParametrosPadroes(args);
        if (args[0].equals("1")) {
            obterParametrosVetorizado(args);
        } else {
            obterParametrosOriginal(args);
        }

        System.out.println("ARQUIVO_TRIPLAS: " + ARQUIVO_TRIPLAS);
        System.out.println("ARQUIVO_REGRAS: " + ARQUIVO_REGRAS);
        System.out.println("EXECUCOES: " + EXECUCOES);
        if (args[0].equalsIgnoreCase("1")) {
            System.out.println("TRIPLAS_LOG: " + TRIPLAS_LOG);
            System.out.println("REGRAS_LOG: " + REGRAS_LOG);
            System.out.println("REGRAS_PRINTAR: " + REGRAS_PRINTAR);
            System.out.println("PRINTAR_TRIPLAS_AND_REGRAS: " + PRINTAR_TRIPLAS_AND_REGRAS);
            System.out.println("FINDER_AUDITAR: " + FINDER_AUDITAR);
            System.out.println("DISPOSITIVO_CODIGO: " + DISPOSITIVO_CODIGO);
            System.out.print("  (" + ((DISPOSITIVO_CODIGO == -1) ? "Melhor Dispositivo" : "") + ")");
            System.out.println("");

            Triples triples = new Triples();
            triples.setMostrarLogs(TRIPLAS_LOG);
            //
            //
            System.out.println("");
            System.out.println("");
            System.out.println("========> Processando triplas");
            System.out.println("==> arquivo: \"" + ARQUIVO_TRIPLAS + "\"");
            triples.processarRegras(ARQUIVO_TRIPLAS);
            //
            //
            Rules rules = new Rules();
            rules.setArrayDeNos(triples.getArrayDeNos());
            rules.setMostrarLogs(REGRAS_LOG);
            System.out.println("");
            System.out.println("");
            System.out.println("========> Processando Regras");
            System.out.println("==> arquivo: \"" + ARQUIVO_REGRAS + "\"");
            rules.processarRegras(ARQUIVO_REGRAS, REGRAS_PRINTAR);
            //
            //
            System.out.println("");
            System.out.println("");
            if (PRINTAR_TRIPLAS_AND_REGRAS) {
                System.out.println("");
                System.out.println("Nós das triplas + regras");
                Triples.listaNos(rules.getArrayDeNos());
            }
            System.out.println("");
            System.out.println("");
//        System.out.println("Mapa de relação das regras");
//        rules.printaTodoMapa();
//        System.out.println("\n\n");
            //
            //
            FinderTestesKernel1 testes = new FinderTestesKernel1(triples, rules);
            testes.setAuditarResultados(FINDER_AUDITAR);
            System.out.println("\n========> Processando busca");
            testes.executarTestes(EXECUCOES, DISPOSITIVO_CODIGO);

        } else {
            Original.TesteTime.executaOriginal(ARQUIVO_TRIPLAS, ARQUIVO_REGRAS, EXECUCOES);
        }
    }

    private static Boolean stringToBoolean(String bool) {
        return bool.equalsIgnoreCase("true");
    }

    private static void obterParametrosVetorizado(String[] args) throws Exception {
        if (args.length < 5) {
            throw new Exception("TRIPLAS_LOG(5) não informado.");
        }
        if (args.length < 6) {
            throw new Exception("REGRAS_LOG(6) não informado.");
        }
        if (args.length < 7) {
            throw new Exception("REGRAS_PRINTAR(7) não informado.");
        }
        if (args.length < 7) {
            throw new Exception("PRINTAR_TRIPLAS_AND_REGRAS(7) não informado.");
        }
        if (args.length < 9) {
            throw new Exception("FINDER_AUDITAR(8) não informado.");
        }
        TRIPLAS_LOG = stringToBoolean(args[4]);
        REGRAS_LOG = stringToBoolean(args[5]);
        REGRAS_PRINTAR = stringToBoolean(args[6]);
        PRINTAR_TRIPLAS_AND_REGRAS = stringToBoolean(args[7]);
        FINDER_AUDITAR = stringToBoolean(args[8]);
        if (args.length == 10) {
            DISPOSITIVO_CODIGO = obtemCodigoDispositivo(args[9]);
        } else {
            DISPOSITIVO_CODIGO = -1;
        }
    }

    private static void obterParametrosOriginal(String[] args) {

    }

    private static void obterParametrosPadroes(String[] args) throws Exception {
        if (args.length == 1) {
            throw new Exception("ARQUIVO_TRIPLAS(2) não informado.");
        }
        if (args.length == 2) {
            throw new Exception("ARQUIVO_REGRAS(3) não informado.");
        }
        if (args.length == 3) {
            throw new Exception("EXECUCOES(4) não informado.");
        }
        ARQUIVO_TRIPLAS = args[1];
        ARQUIVO_REGRAS = args[2];
        try {
            EXECUCOES = Integer.valueOf(args[3]);
        } catch (Exception e) {
            throw new Exception("Número de execuçõs deve ser um número inteiros.");
        }
    }

    private static Integer obtemCodigoDispositivo(String arg) throws Exception {
        Integer codigo = -1;
        if (arg != null && !arg.equals("")) {
            try {
                codigo = Integer.valueOf(arg);
            } catch (Exception e) {
                throw new Exception("Codigo do driver informado deve ser um número inteiro.");
            }
        }
        return codigo;
    }
}
