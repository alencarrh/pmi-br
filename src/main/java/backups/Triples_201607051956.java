package backups;

import Classes.TriplasRDF;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.util.FileManager;
import org.bridj.Pointer;

/**
 *
 * @author Alencar Rodrigo Hentges <alencarhentges@gmail.com>
 */
public class Triples_201607051956 {

    private static Long tempoInicio;
    private static Long tempoFim;
    private Boolean mostrarLogs;
    private static final int DIV_TIME = 1000000;
    private Pointer<Integer> pointerTriplaSubject;
    private Pointer<Integer> pointerTriplaPredicate;
    private Pointer<Integer> pointerTriplaObject;
    private ArrayList<TriplasRDF> arrayDeNos;
    private Integer numeroDeTriplas;

    public Triples_201607051956() {

    }

    public Triples_201607051956(ArrayList<TriplasRDF> arrayDeNos) {
        this.arrayDeNos = arrayDeNos;

    }

    //<editor-fold defaultstate="collapsed" desc="Gets e Sets"> 
    public Pointer<Integer> getPointerTriplaSubject() {
        return pointerTriplaSubject;
    }

    public Pointer<Integer> getPointerTriplaPredicate() {
        return pointerTriplaPredicate;
    }

    public Pointer<Integer> getPointerTriplaObject() {
        return pointerTriplaObject;
    }

    public ArrayList<TriplasRDF> getArrayDeNos() {
        return arrayDeNos;
    }

    public void setMostrarLogs(Boolean mostrarLogs) {
        this.mostrarLogs = mostrarLogs;
    }

    public Integer getNumeroDeTriplas() {
        return numeroDeTriplas;
    }

    //</editor-fold>
    public void processarRegras(String arquivo) throws IllegalArgumentException {

        Model model = ModelFactory.createDefaultModel();
        if (mostrarLogs) {
            System.out.print("Lendo Triplas... ");
            tempoInicio = System.nanoTime();
        }
        //Lendo Arquivo
        InputStream in = FileManager.get().open(arquivo);
        if (in == null) {
            throw new IllegalArgumentException("Arquivo \"" + arquivo + "\" não encontrado");
        }
        //lendo o arquivo RDF/XML
        model.read(in, "");

        if (mostrarLogs) {
            tempoFim = System.nanoTime();
            System.out.print("[" + (tempoFim - tempoInicio) + " ns]~[" + (tempoFim - tempoInicio) / DIV_TIME + " ms]\n");
        }
        if (arrayDeNos == null) {
            arrayDeNos = new ArrayList<>();
        }
        processarRegras(model);
    }

    private void processarRegras(Model model) {

        //Alocando memória para ponteiros
        if (mostrarLogs) {
            System.out.print("Alocando memória para ponteiros... ");
            tempoInicio = System.nanoTime();
        }

        List<Statement> listTriplas = model.listStatements().toList();
        numeroDeTriplas = listTriplas.size();

        pointerTriplaSubject = Pointer.allocateInts(numeroDeTriplas);
        pointerTriplaPredicate = Pointer.allocateInts(numeroDeTriplas);
        pointerTriplaObject = Pointer.allocateInts(numeroDeTriplas);

        if (mostrarLogs) {
            tempoFim = System.nanoTime();
            System.out.print("[" + (tempoFim - tempoInicio) + " ns]~[" + (tempoFim - tempoInicio) / DIV_TIME + " ms]\n");
            System.out.print("Vetorizando Triplas... ");
            tempoInicio = System.nanoTime();
        }

        int id;
        for (int i = 0; i < numeroDeTriplas; i++) {
            Resource subject = listTriplas.get(i).getSubject();   //pega o sujeito da tripla
            Property predicate = listTriplas.get(i).getPredicate();   //pega o predicado da tripla
            RDFNode object = listTriplas.get(i).getObject();  //pega o objeto da tripla

            id = arrayDeNos.indexOf(new Classes.Sujeito(0, subject));
            if (id == -1) {
                arrayDeNos.add(new Classes.Sujeito(arrayDeNos.size() + 1, subject));
                pointerTriplaSubject.set(i, arrayDeNos.size());
            } else {
                pointerTriplaSubject.set(i, arrayDeNos.get(id).id);
            }

            id = arrayDeNos.indexOf(new Classes.Predicado(0, predicate));
            if (id == -1) {
                arrayDeNos.add(new Classes.Predicado(arrayDeNos.size() + 1, predicate));
                pointerTriplaPredicate.set(i, arrayDeNos.size());
            } else {
                pointerTriplaPredicate.set(i, arrayDeNos.get(id).id);
            }

            id = arrayDeNos.indexOf(new Classes.Objeto(0, object));
            if (id == -1) {
                arrayDeNos.add(new Classes.Objeto(arrayDeNos.size() + 1, object));
                pointerTriplaObject.set(i, arrayDeNos.size());
            } else {
                pointerTriplaObject.set(i, arrayDeNos.get(id).id);
            }
        }
        if (mostrarLogs) {
            tempoFim = System.nanoTime();
            System.out.print("[" + (tempoFim - tempoInicio) + " ns]~[" + (tempoFim - tempoInicio) / DIV_TIME + " ms]\n");
        }
    }

    public static void listaNos(ArrayList<TriplasRDF> arrayDeNos) {
        if (arrayDeNos == null || arrayDeNos.isEmpty()) {
            System.out.println("[empty]");
            return;
        }
        TriplasRDF tripla;
        for (int i = 0; i < arrayDeNos.size(); i++) {
            tripla = arrayDeNos.get(i);
            System.out.println("[" + tripla.id + "] " + tripla.toString());
        }
    }

}
