package backups;

import Finder.FinderTestesKernel1;
import Rules.Rules;
import Triples.Triples;

import java.io.IOException;

/**
 *
 * @author Alencar Rodrigo Hentges <alencarhentges@gmail.com>
 * @date 26/06/2016 - 16:14:21
 */
public class Main_201605072005 {

//    private static final String ARQUIVO_TRIPLAS = "storefront.owl";
    private static final String ARQUIVO_TRIPLAS = "fr.rdf";
//    private static final String ARQUIVO_REGRAS = "storefront.rules";
    private static final String ARQUIVO_REGRAS = "owl-fb.rules";

    public static void main(String[] args) throws IOException {

        Triples triples = new Triples();
        triples.setMostrarLogs(Boolean.TRUE);
        //
        //
        System.out.println("\n\n========> Processando triplas");
        System.out.println("==> arquivo: \"" + ARQUIVO_TRIPLAS + "\"");
        triples.processarRegras(ARQUIVO_TRIPLAS);
        //
        //
        Rules rules = new Rules();
        rules.setArrayDeNos(triples.getArrayDeNos());
        rules.setMostrarLogs(Boolean.TRUE);
        System.out.println("\n\n========> Processando Regras");
        System.out.println("==> arquivo: \"" + ARQUIVO_REGRAS + "\"");
        rules.processarRegras(ARQUIVO_REGRAS, Boolean.TRUE);
        //
        //
//        System.out.println("\n\n");
//        System.out.println("Nós das triplas + regras");
//        Triples.listaNos(rules.getArrayDeNos());
//        System.out.println("\n");
//        System.out.println("Mapa de relação das regras");
//        rules.printaTodoMapa();
//        System.out.println("\n\n");
        //
        //
        FinderTestesKernel1 testes = new FinderTestesKernel1(triples, rules);
        testes.setAuditarResultados(Boolean.TRUE);
        System.out.println("========> Processando busca");
        testes.executarTestes(30, -1);
    }
}
