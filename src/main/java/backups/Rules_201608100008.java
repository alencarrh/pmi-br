package backups;

import Classes.TriplasRDF;
import Classes.TriplaInteger;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.apache.jena.reasoner.TriplePattern;
import org.apache.jena.reasoner.rulesys.ClauseEntry;
import org.apache.jena.reasoner.rulesys.Functor;
import org.apache.jena.reasoner.rulesys.GenericRuleReasoner;
import org.apache.jena.reasoner.rulesys.Rule;

/**
 *
 * @author Alencar Rodrigo Hentges <alencarhentges@gmail.com>
 */
public class Rules_201608100008 {

    private static Long tempoInicio;
    private static Long tempoFim;
    private Boolean mostrarLogs;
    private static final int DIV_TIME = 1000000;
    private Integer numeroDeRegras;
    private ArrayList<TriplasRDF> arrayDeNos;
    private final Map<Integer, ArrayList<TriplaInteger>> triplasInteger;

    public Rules_201608100008() {
        triplasInteger = new TreeMap<>();
    }

    //<editor-fold defaultstate="collapsed" desc="Gets e Sets"> 
    public ArrayList<TriplasRDF> getArrayDeNos() {
        return arrayDeNos;
    }

    public Map<Integer, ArrayList<TriplaInteger>> getTriplasInteger() {
        return triplasInteger;
    }

    public void setMostrarLogs(Boolean mostrarLogs) {
        this.mostrarLogs = mostrarLogs;
    }

    public void setArrayDeNos(ArrayList<TriplasRDF> arrayDeNos) {
        this.arrayDeNos = arrayDeNos;
    }

    public Integer getNumeroDeRegras() {
        return numeroDeRegras;
    }

    //</editor-fold>
    /**
     * Inicializa as estruturas de armazenamento e percorre as regras
     *
     * @param arquivo
     * @param printarRules
     * @throws IllegalArgumentException
     */
    public void processarRegras(String arquivo, Boolean printarRules) throws IllegalArgumentException {

        if (mostrarLogs) {
            System.out.print("Leitura das regras... ");
            tempoInicio = System.nanoTime();
        }
        File f = new File(arquivo);
        if (!f.exists()) {
            throw new IllegalArgumentException("Arquivo \"" + arquivo + "\" não encontrado");
        }

        List listaDeRegras = Rule.rulesFromURL(arquivo);
        GenericRuleReasoner reasoner = new GenericRuleReasoner(listaDeRegras);
        List<Rule> regras = reasoner.getRules();

        if (mostrarLogs) {
            tempoFim = System.nanoTime();
            System.out.print("[" + (tempoFim - tempoInicio) + " ns]~[" + (tempoFim - tempoInicio) / DIV_TIME + " ms]");
            System.out.println("");
            if (printarRules) {
                System.out.println("Vetorizando regras e criando padrões... ");
            } else {
                System.out.print("Vetorizando regras e criando padrões... ");
            }
            tempoInicio = System.nanoTime();
        }
        numeroDeRegras = regras.size();
        for (int i = 0; i < regras.size(); i++) {
            Rule regra = regras.get(i);
            processaRegra(regra, i);
            if (mostrarLogs && printarRules) {
                System.out.println((i + 1) + ". >>> " + regra.toString());
                printPadraoRegra(i);
                System.out.println("");
            }
        }
        if (mostrarLogs) {
            tempoFim = System.nanoTime();
            if (printarRules) {
                System.out.print("Vetorizando regras e criando padrões... ");
            }
            System.out.print("[" + (tempoFim - tempoInicio) + " ns]~[" + (tempoFim - tempoInicio) / DIV_TIME + " ms]");
            System.out.println("");
        }
    }

    /**
     * Faz a chamada do método principal de criação dos padrões de busca
     *
     * @param regra
     * @param id
     */
    private void processaRegra(Rule regra, int id) {
        criaPadraoLHS(regra, id);
    }

    /**
     * Verifica o tipo de regra e leva para o método adequado. Caso todas as
     * condições foram falsas, busca possíveis URIs para adicionar ao array de
     * URIs
     *
     * @param regra
     * @param id
     */
    private void criaPadraoLHS(Rule regra, int id) {
        if (regra.isAxiom()) {
            return;
        }
        if (regra.isBackward()) {
            criaPadraoLHSBackward(regra, id);
            return;
        }
        if (regra.isMonotonic()) {
            criaPadraoLHSForward(regra, id);
            return;
        }
        //não é nenhum dos tipos acima
        adicionaURIsApartirDaRegra(regra);
    }

    /**
     * Cria o padrão de regras backward.
     *
     * @param regra
     * @param id
     */
    private void criaPadraoLHSBackward(Rule regra, int id) {
        // não suportado ainda.
    }

    /**
     * Cria o padrão de regras forward.
     *
     * @param regra
     * @param id
     */
    private void criaPadraoLHSForward(Rule regra, int id) {
        ArrayList<TriplaInteger> triplaInteger = new ArrayList<>();
        ClauseEntry elementosBody[] = regra.getBody();
        for (ClauseEntry elemento : elementosBody) {
            if (elemento instanceof TriplePattern) {
                tratarTripla((TriplePattern) elemento, triplaInteger);
            } else if (elemento instanceof Functor) {
                //tratar função - não suportado ainda.
            }
        }
        triplasInteger.put(id, triplaInteger);
    }

    /**
     * Cria um tripla númerica baseada na tripla de entrada.
     *
     * @param tripla - tripla original
     * @param regraEmNumeros - ArrayList que será salva as regras
     */
    private void tratarTripla(TriplePattern tripla, ArrayList<TriplaInteger> regraEmNumeros) {
        TriplaInteger triplaInteger = new TriplaInteger();
        triplaInteger.setSujeito(tratarSubject(tripla.getSubject()));
        triplaInteger.setPredicado(tratarPredicate(tripla.getPredicate()));
        triplaInteger.setObjeto(tratarObject(tripla.getObject()));

        regraEmNumeros.add(triplaInteger);
    }

    /**
     * Faz a adição das URIs da tripla para o array de URIs
     *
     * @param tripla - tripla original
     */
    private void tratarTripla(TriplePattern tripla) {
        tratarSubject(tripla.getSubject());
        tratarPredicate(tripla.getPredicate());
        tratarObject(tripla.getObject());
    }

    /**
     * Retorna o ID do sujeito ou 0 se for uma variável.
     *
     * @param subject - sujeito
     * @return ID do sujeito
     */
    private int tratarSubject(org.apache.jena.graph.Node subject) {
        if (subject.isVariable()) {
            return 0;
        }
        if (subject.isURI()) {
            return adicionarURI(subject.getURI());
        }
        return 0;
    }

    /**
     * Retorna o ID do predicado ou 0 se for uma variável.
     *
     * @param predicate - predicado
     * @return ID do predicado
     */
    private int tratarPredicate(org.apache.jena.graph.Node predicate) {
        if (predicate.isVariable()) {
            return 0;
        }
        if (predicate.isURI()) {
            return adicionarURI(predicate.getURI());
        }
        return 0;
    }

    /**
     * Retorna o ID do objeto ou 0 se for uma variável.
     *
     * @param object - objeto
     * @return ID do objeto
     */
    private int tratarObject(org.apache.jena.graph.Node object) {
        if (object.isVariable()) {
            return 0;
        }
        if (object.isURI()) {
            return adicionarURI(object.getURI());
        }
        return 0;
    }

    /**
     * Adiciona uma novo URI no array de URIs
     *
     * @param no - URI à ser adicionada
     * @return ID da URI
     */
    private int adicionarURI(String no) {
        int id = arrayDeNos.indexOf(new Classes.Node(0, no));
        if (id == -1) {
            id = arrayDeNos.size() + 1;
            arrayDeNos.add(new Classes.Node(id, no));
        } else {
            id = arrayDeNos.get(id).id;
        }
        return id;
    }

    /**
     * Adiciona as URIs de um regra ao array de URIs
     *
     * @param regra - regra original
     */
    private void adicionaURIsApartirDaRegra(Rule regra) {
        ClauseEntry elementosBody[] = regra.getBody();
        for (ClauseEntry elemento : elementosBody) {
            if (elemento instanceof TriplePattern) {
                tratarTripla((TriplePattern) elemento);
            }
        }
    }

    /**
     * Printa os padrões de uma regra.
     *
     * @param id
     */
    private void printPadraoRegra(int id) {
        if (!triplasInteger.containsKey(id)) {
            System.out.println((id + 1) + ". >>> null");
        } else {
            System.out.print((id + 1) + ". >>>  ");
            for (TriplaInteger triplaInteger : triplasInteger.get(id)) {
                System.out.print(triplaInteger + " ");
            }
            System.out.println("");
        }
    }

    public void printaTodoMapa() {
        for (Map.Entry<Integer, ArrayList<TriplaInteger>> entry : triplasInteger.entrySet()) {
            System.out.print("Regra [" + (entry.getKey() + 1) + "] = ");
            for (TriplaInteger triplasInteger : entry.getValue()) {
                System.out.print(triplasInteger + " ");
            }
            System.out.println("");
        }
    }
}
