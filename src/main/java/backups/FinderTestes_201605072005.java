package backups;

import Classes.TriplaInteger;
import Rules.Rules;
import Triples.Triples;
import com.alencar.JenaTests.TutorialKernels;
import com.nativelibs4java.opencl.*;
import com.nativelibs4java.opencl.CLMem.Usage;
import org.bridj.Pointer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import static org.bridj.Pointer.allocateInts;

public class FinderTestes_201605072005 {

    private Boolean auditarResultados;
    private static Long tempoInicio;
    private static final int DIV_TIME = 1000000;

    private final Triples triples;
    private final Rules rules;

    private final Pointer<Integer> subPtr, predPtr, objPtr, findPtr, maskPtr;
    private Pointer<Integer> outPtr;

    private CLBuffer<Integer> sub, pred, obj, mask, find;
    private CLBuffer<Integer> out;
    private CLQueue queue;
    private CLContext context;

    public FinderTestes_201605072005(Triples triples, Rules rules) {
        this.triples = triples;
        this.rules = rules;
        this.subPtr = triples.getPointerTriplaSubject();
        this.predPtr = triples.getPointerTriplaPredicate();
        this.objPtr = triples.getPointerTriplaObject();
        findPtr = allocateInts(3);
        maskPtr = allocateInts(3);
        auditarResultados = true;
    }

    public void setAuditarResultados(Boolean auditarResultados) {
        this.auditarResultados = auditarResultados;
    }

    public void executarTestes(Integer nExperimentos) throws IOException {
        context = escolherDevice();
        queue = context.createDefaultQueue();
        ArrayList<TriplaInteger> test_Triplas = new ArrayList<>();

        long numeroDeTriplas = triples.getNumeroDeTriplas();
        long pos;
        //tipo 1 ----------------------------------------------------------
        //
        //tripla (any, any, any)
        TriplaInteger test_temp = new TriplaInteger();
        test_temp.setSujeito(0);
        test_temp.setPredicado(0);
        test_temp.setObjeto(0);
        //adiciona tripla 1
        test_Triplas.add(test_temp);
        //
        //tipo 1 ----------------------------------------------------------
        //
        //tipo 2 ----------------------------------------------------------
        //
        //tripla (ALEATÓRIO, any, any)
        test_temp = new TriplaInteger();
        test_temp.setSujeito(subPtr.getIntAtIndex((long) (0 + Math.random() * numeroDeTriplas)));
        test_temp.setPredicado(0);
        test_temp.setObjeto(0);
        //adiciona tripla 2.1
        test_Triplas.add(test_temp);
        //
        //tripla (any, ALEATÓRIO, any)
        test_temp = new TriplaInteger();
        test_temp.setSujeito(0);
        test_temp.setPredicado(predPtr.getIntAtIndex((long) (0 + Math.random() * numeroDeTriplas)));
        test_temp.setObjeto(0);
        //adiciona tripla 2.2
        test_Triplas.add(test_temp);
        //
        //tripla (any, any, ALEATÓRIO)
        test_temp = new TriplaInteger();
        test_temp.setSujeito(0);
        test_temp.setPredicado(0);
        test_temp.setObjeto(objPtr.getIntAtIndex((long) (0 + Math.random() * numeroDeTriplas)));
        //adiciona tripla 2.3
        test_Triplas.add(test_temp);
        //
        //tipo 2 ----------------------------------------------------------
        //
        //tipo 3 ----------------------------------------------------------
        //
        //tripla (ALEATÓRIO, ALEATÓRIO, any)
        test_temp = new TriplaInteger();
        pos = (long) (0 + Math.random() * numeroDeTriplas);
        test_temp.setSujeito(subPtr.getIntAtIndex(pos));
        test_temp.setPredicado(predPtr.getIntAtIndex(pos));
        test_temp.setObjeto(0);
        //adiciona tripla 3.1
        test_Triplas.add(test_temp);
        //
        //tripla (ALEATÓRIO, any, ALEATÓRIO)
        test_temp = new TriplaInteger();
        pos = (long) (0 + Math.random() * numeroDeTriplas);
        test_temp.setSujeito(subPtr.getIntAtIndex(pos));
        test_temp.setPredicado(0);
        test_temp.setObjeto(objPtr.getIntAtIndex(pos));
        //adiciona tripla 3.2
        test_Triplas.add(test_temp);
        //tripla (any, ALEATÓRIO, ALEATÓRIO)
        test_temp = new TriplaInteger();
        pos = (long) (0 + Math.random() * numeroDeTriplas);
        test_temp.setSujeito(0);
        test_temp.setPredicado(predPtr.getIntAtIndex(pos));
        test_temp.setObjeto(objPtr.getIntAtIndex(pos));
        //adiciona tripla 3.3
        test_Triplas.add(test_temp);

        //
        //tipo 3 ----------------------------------------------------------
        //
        //tripla (ALEATÓRIO, ALEATÓRIO, ALEATÓRIO)
        test_temp = new TriplaInteger();
        pos = (long) (0 + Math.random() * numeroDeTriplas);
        test_temp.setSujeito(subPtr.getIntAtIndex(pos));
        test_temp.setPredicado(predPtr.getIntAtIndex(pos));
        test_temp.setObjeto(objPtr.getIntAtIndex(pos));

        //adiciona tripla 4
        test_Triplas.add(test_temp);

        System.out.println("Iniciando testes ---------------------------------------------");
        System.out.println("Número de triplas: " + numeroDeTriplas);
        System.out.println("Número de  regras: " + rules.getNumeroDeRegras());
        for (TriplaInteger test_Tripla : test_Triplas) {
            setarPadraoEMascara(test_Tripla);
            System.out.println("\n==================> Padrão de busca.: " + test_Tripla + "");
            System.out.println("==================> Máscara de busca: (" + maskPtr.get(0) + " " + maskPtr.get(1) + " " + maskPtr.get(2) + ")");
            long total = 0;
            for (int j = 0; j < nExperimentos; j++) {
                long umaBusca = buscarPadrao();
                System.out.print("[" + umaBusca + " ns]~[" + (umaBusca / DIV_TIME) + " ms] | ");
                total += umaBusca;
            }
            if (auditarResultados) {
                System.out.print("\nAuditando resultados...  ");
                long[] resultados = auditarResultados();
                System.out.print("[" + resultados[0] + " ns]~[" + (resultados[0] / DIV_TIME) + "ms]");
                System.out.println("\nMatches: " + resultados[1] + "");
            }
            System.out.println("\nMédia dos " + nExperimentos + " experimentos: [" + (total / nExperimentos) + " ns] ~ [" + ((total / nExperimentos) / DIV_TIME) + " ms]");

        }
    }

    /**
     * Faz a chamada do código do OpenCL.
     *
     * @return tempo de execução em nanosegundos
     * @throws IOException
     */
    private long buscarPadrao() throws IOException {
        TutorialKernels kernels = new TutorialKernels(context);
        int[] globalSizes = new int[]{triples.getNumeroDeTriplas()};
        CLEvent addEvt;
        tempoInicio = System.nanoTime();
//        addEvt = kernels.matchPattern(queue, sub, pred, obj, mask, find, out, triples.getNumeroDeTriplas(), globalSizes, null);
//        outPtr = out.read(queue, addEvt); // blocks until matchPattern finished
        return (System.nanoTime() - tempoInicio);
    }

    /**
     * Define o padrão e a mascara de busca
     *
     * @param tripla - TriplaInteger com o padrão de busca.
     */
    private void setarPadraoEMascara(TriplaInteger tripla) {
        setarPadraoBusca(tripla);
        setarMascara(tripla);
        prepararOpenCL();
    }

    /**
     * Define o padrão de busca.
     *
     * @param tripla
     */
    private void setarPadraoBusca(TriplaInteger tripla) {
        findPtr.set(0, tripla.getSujeito());
        findPtr.set(1, tripla.getPredicado());
        findPtr.set(2, tripla.getObjeto());
    }

    /**
     *
     * @param tripla
     */
    private void setarMascara(TriplaInteger tripla) {
        maskPtr.set(0, tripla.getSujeito() == 0 ? 0 : 1);
        maskPtr.set(1, tripla.getPredicado() == 0 ? 0 : 1);
        maskPtr.set(2, tripla.getObjeto() == 0 ? 0 : 1);
    }

    private void prepararOpenCL() {
        criarBuffersDeEntrada();
        criarBuffersDeRetorno();
    }

    private void criarBuffersDeEntrada() {
        //criando buffers de entrada
        sub = context.createIntBuffer(Usage.Input, subPtr);
        pred = context.createIntBuffer(Usage.Input, predPtr);
        obj = context.createIntBuffer(Usage.Input, objPtr);
        mask = context.createIntBuffer(Usage.Input, maskPtr);
        find = context.createIntBuffer(Usage.Input, findPtr);
    }

    private void criarBuffersDeRetorno() {
        //criando buffer de retorno
        out = context.createIntBuffer(Usage.Output, triples.getNumeroDeTriplas());
    }

    private CLContext escolherDevice() {
        List<CLDevice> devicesParaEscolher = new ArrayList<>();
        for (CLPlatform platform : com.nativelibs4java.opencl.JavaCL.listPlatforms()) {
            devicesParaEscolher.addAll(Arrays.asList(platform.listAllDevices(false)));
        }

        //seleciona o dispositivo
        CLDevice deviceSelecionado = selecionarDevice(devicesParaEscolher);

        //cria o contexto atual com o drive selecionado.
        CLContext tempContext;
        if (deviceSelecionado == null) {
            tempContext = com.nativelibs4java.opencl.JavaCL.createBestContext();
        } else {
            tempContext = com.nativelibs4java.opencl.JavaCL.createContext(null, deviceSelecionado);
        }

        //printa o nome do dispositivo escolhido...
        System.out.println("Dispositivo selecionado: " + tempContext.getDevices()[0].getName() + "\n");

        return tempContext;
    }

    private CLDevice selecionarDevice(List<CLDevice> devices) {
        Scanner ler = new Scanner(System.in);
        System.out.println("Dispositivo encontrados: ");
        int i = 0;
        for (CLDevice device : devices) {
            System.out.println((++i) + " -> " + device.getName());
        }

        Integer op = 0;
        do {
            try {
                System.out.print("\n(-1 default)Escolha um: ");
                String index = ler.next();
                op = Integer.valueOf(index);
                if (op == -1) {
                    return null;
                }
            } catch (NumberFormatException e) {
                System.out.println("Digite apenas números!");
            }
        } while (op < 1 || op > devices.size());
        return devices.get((op - 1));
    }

    private long[] auditarResultados() {
        long ind, countMatch = 0;
        Boolean isEqual[] = new Boolean[3];
        String comp[] = new String[3];
        tempoInicio = System.nanoTime();
        for (ind = 0; ind < triples.getNumeroDeTriplas(); ind++) {
            isEqual[0] = subPtr.getIntAtIndex(ind) == findPtr.getIntAtIndex(0);
            isEqual[1] = predPtr.getIntAtIndex(ind) == findPtr.getIntAtIndex(1);
            isEqual[2] = objPtr.getIntAtIndex(ind) == findPtr.getIntAtIndex(2);
            if (outPtr.getIntAtIndex(ind) == 0) {
                countMatch++;
                boolean ok = true;
                if (maskPtr.getIntAtIndex(0) == 1) {
                    ok = isEqual[0];
                }
                if (ok && maskPtr.getIntAtIndex(1) == 1) {
                    ok = isEqual[1];
                }
                if (ok && maskPtr.getIntAtIndex(2) == 1) {
                    ok = isEqual[2];
                }
                if (!ok) {
                    comp[0] = maskPtr.get(0) == 1 ? isEqual[0].toString() : "---";
                    comp[1] = maskPtr.get(1) == 1 ? isEqual[1].toString() : "---";
                    comp[2] = maskPtr.get(2) == 1 ? isEqual[2].toString() : "---";
                    System.out.println("Problemas:");
                    System.out.println("Tripla\t" + subPtr.get(ind) + "\t" + predPtr.get(ind) + "\t" + objPtr.get(ind));
                    System.out.println("Compar\t" + comp[0] + "\t" + comp[1] + "\t" + comp[2]);
                    System.out.println("Output  " + outPtr.get(ind) + "\n");
                }
            } else {
                boolean ok = false;
                if (maskPtr.getIntAtIndex(0) == 1) {
                    ok = !isEqual[0];
                }
                if (!ok && maskPtr.getIntAtIndex(1) == 1) {
                    ok = !isEqual[1];
                }
                if (!ok && maskPtr.getIntAtIndex(2) == 1) {
                    ok = !isEqual[2];
                }
                if (!ok) {
                    comp[0] = maskPtr.get(0) == 1 ? isEqual[0].toString() : "---";
                    comp[1] = maskPtr.get(1) == 1 ? isEqual[1].toString() : "---";
                    comp[2] = maskPtr.get(2) == 1 ? isEqual[2].toString() : "---";
                    System.out.println("Problemas:");
                    System.out.println("Tripla\t" + subPtr.get(ind) + "\t" + predPtr.get(ind) + "\t" + objPtr.get(ind));
                    System.out.println("Compar\t" + comp[0] + "\t" + comp[1] + "\t" + comp[2]);
                    System.out.println("Output  " + outPtr.get(ind) + "\n");
                }
            }
        }
        long tempo = (System.nanoTime() - tempoInicio);
        long retorno[] = new long[2];
        retorno[0] = tempo;
        retorno[1] = countMatch;
        return retorno;
    }
}
