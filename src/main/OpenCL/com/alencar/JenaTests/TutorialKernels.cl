__kernel void matchPatternKernel1(//código Denis(1)
        __global const int* sub, 
        __global const int* pre, 
        __global const int* obj,
        __global const int* pat, 
        __global int* out, 
        int n
){
    int i = get_global_id(0);
    if (i >= n)
        return;

        out[i]  = (sub[i] - pat[0]) * pat[0];
        out[i] += (pre[i] - pat[1]) * pat[1];
        out[i] += (obj[i] - pat[2]) * pat[2];

}

__kernel void matchPatternKernel2(//código Denis(2)
        __global const int* sub, 
        __global const int* pre, 
        __global const int* obj,
        __global const int* pat, 
        __global bool* out, 
        int n
){
    int i = get_global_id(0);
    if (i >= n)
        return;

    int res;
    if(pat[0] != 0) {
        res = sub[i] & pat[0];
        out[i] =  res != sub[i] || res != pat[0];
    }
    if(pat[1] != 0) {
        res = pre[i] & pat[1];
        out[i] += res != pre[i] || res != pat[1];
    }
    if(pat[2] != 0) {
        res = obj[i] & pat[2];
        out[i] += res != obj[i] || res != pat[2];
    }
}


__kernel void matchPatternKernel3(//ccódigo meu
        __global const int* sub, 
        __global const int* pre, 
        __global const int* obj,
        __global const int* pat, 
        __global bool* out, 
        int n
){
    int i = get_global_id(0);
    if (i >= n)
        return;

    out[i]=(pat[0] == 0 || pat[0] == sub[i]) && (pat[2] == 0 || pat[2] == obj[i]) && (pat[1] == 0 || pat[1] == pre[i]);
    //out[i]= (pat[0] == 0 || pat[0] == sub[i]) && (pat[1] == 0 || pat[1] == pre[i]) && (pat[2] == 0 || pat[2] == obj[i]);
}

__kernel void matchPatternKernel4(//primeiro código
        __global const int* sub,
        __global const int* pre,
        __global const int* obj,
        __global const int* mask,
        __global const int* pat, 
        __global int* out,
         int n
){
    int i = get_global_id(0);
    if (i >= n)
        return;

    out[i]=0;
    if(mask[0] == 1)
        out[i] = abs(sub[i]-pat[0]);
    if(mask[1] == 1)
        out[i] += abs(pre[i]-pat[1]);
    if(mask[2] == 1)
        out[i] += abs(obj[i]-pat[2]);

}

__kernel void DefineAllTo1(//teste sem nenhuma execução
        __global int* out,
        int n
){
    int i = get_global_id(0);
    if (i >= n)
        return;
    out[i] = 1;        
}



