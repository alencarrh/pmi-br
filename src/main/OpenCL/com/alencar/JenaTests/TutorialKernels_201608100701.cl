__kernel void matchPattern(
        __global const int* sub, __global const int* pre, __global const int* obj,
        __global const int* mask,  __global const int* pat, 
        __global int* out, int n) 
{
    int i = get_global_id(0);
    if (i >= n)
        return;


        
        

        out[i] = (pat[0] == sub[i]) ? 0 : (1 * mask[0]);
        out[i] += (pat[1] == pre[i]) ? 0 : (1 * mask[1]);
        out[i] += (pat[2] == obj[i]) ? 0 : (1 * mask[2]);

}